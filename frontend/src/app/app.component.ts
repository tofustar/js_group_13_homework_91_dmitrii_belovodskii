import {AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";

interface Pixel {
  x: number,
  y: number,
  color: string,
}

interface ServerMessage {
  type: string,
  coordinates: Pixel[],
  pixelCoordinates: Pixel,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements AfterViewInit, OnDestroy {
  ws!: WebSocket;
  @ViewChild('canvas') canvas!: ElementRef;
  @ViewChild('f') form!: NgForm;

  ngAfterViewInit() {
    this.ws = new WebSocket('ws://localhost:8000/canvas');
    this.ws.onclose = () => console.log('ws closed');

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if(decodedMessage.type === 'PREV_PIXELS') {
        decodedMessage.coordinates.forEach(c => {
          this.drawPixel(c.x, c.y, c.color);
        });
      }

      if(decodedMessage.type === 'NEW_PIXEL') {
        const {x, y, color} = decodedMessage.pixelCoordinates;
        this.drawPixel(x, y, color);
      }
    }
  }

  ngOnDestroy() {
    this.ws.close();
  }

  drawPixel(x: number, y: number, color: string) {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    const ctx = canvas.getContext('2d')!;
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.arc(x, y, 10, 0, 2 * Math.PI);
    ctx.stroke();
  }

  onCanvasClick(event: MouseEvent) {
    const x = event.offsetX;
    const y = event.offsetY;
    let color = this.form.value.color;

    if(!color) {
      color = 'black';
    }

    this.ws.send(JSON.stringify({
      type: 'SEND_PIXEL',
      coordinates: {x, y, color}
    }))
  }
}
